# Introduction

This program reads two FDroid repository files in XML format and compares them. It looks for apps that have been updated. Then it prints a reports about its findings.

# Rationale

I wrote this program to automatically send a message to a Telegram channel informing users that a new update is available (this is done by a simple bash script and is not part of this code repository).

# TO DO

I developed this program back in March 2018 when going by train from Graz to Leoben in a quick and dirty way. In April 2018 I made a few improvements. The design of the application could be improved a bit. By now, it completely lacks any tests.
