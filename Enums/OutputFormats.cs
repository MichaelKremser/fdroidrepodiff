﻿namespace fdroidrepodiff.Enums
{
    /// <summary>
    /// Output formats.
    /// </summary>
    public enum OutputFormats
    {
        /// <summary>
        /// Plain text, suitable when no or only minimal formatting is available
        /// </summary>
        PlainText,
        /// <summary>
        /// The famous HTML format
        /// </summary>
        HTML
    }
}
