﻿using System;
using System.Reflection;
using fdroidrepodiff.Enums;

namespace fdroidrepodiff
{
    class MainClass
    {
        public static NLog.ILogger logger = new NLog.LogFactory().GetLogger(nameof(fdroidrepodiff));

        // arg1: Input file 1 (current repository XML)
        // arg2: Input file 2 (previous repository XML)
        // arg3: Format (html | text)
        // arg4: Output file
        public static int Main(string[] args)
        {
            if (args.Length < 4)
            {
                var assemblyName = Assembly.GetExecutingAssembly().GetName();
                Console.WriteLine($"{assemblyName.Name} {assemblyName.Version} currentxml previousxml [text|html] outputfile");
                return 1;
            }

            var inputFileCurrentRepo = args[0];
            var inputFilePreviousRepo = args[1];
            var format = args[2].ToLower();
            OutputFormats outputFormat = OutputFormats.PlainText;
            if (format == "html")
            {
                outputFormat = OutputFormats.HTML;
            }
            logger.Debug($"Getting updated apps, using format {outputFormat}");
            var outputFile = args[3];

            var mainApplication = new MainApplication(logger);
            mainApplication.Run(inputFileCurrentRepo, inputFilePreviousRepo, outputFormat, outputFile);

            return 0;
        }
    }
}