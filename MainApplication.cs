﻿using System;
using System.IO;
using System.Text;
using fdroidrepodiff.Enums;
using fdroidrepodiff.Generators;

namespace fdroidrepodiff
{
    /// <summary>
    /// The main application that does the actual works.
    /// </summary>
    public class MainApplication
    {
        public NLog.ILogger logger;

        /// <summary>
        /// Generates a new instance of the <see cref="MainApplication"/>.
        /// </summary>
        public MainApplication(NLog.ILogger logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Runs the main application using the arguments specified.
        /// </summary>
        /// <returns>0 if successful, otherwise a positive number.</returns>
        /// <param name="inputFileCurrentRepo">Input file of current repository.</param>
        /// <param name="inputFilePreviousRepo">Input file of previous repository.</param>
        /// <param name="outputFormat">Output format</param>
        /// <param name="outputFile">Output file</param>
        public int Run(string inputFileCurrentRepo, string inputFilePreviousRepo, OutputFormats outputFormat, string outputFile)
        {
            foreach (var inputFile in new[] { inputFileCurrentRepo, inputFilePreviousRepo })
            {
                if (!File.Exists(inputFile))
                {
                    var error = $"Could not read file \"{inputFile}\"!";
                    Console.Error.WriteLine(error);
                    logger.Error(error);
                    return 2;
                }
            }

            var appRepoCurrent = FDroidApplicationRepository.CreateRepositoryFromXml(inputFileCurrentRepo);
            var appRepoPrevious = FDroidApplicationRepository.CreateRepositoryFromXml(inputFilePreviousRepo);
            var updatedApps = appRepoCurrent.GetUpdatedApps(appRepoPrevious);

            var generator = GeneratorFactory.GetAppListGenerator(outputFormat);
            var sb = new StringBuilder();
            sb.Append(generator.GenerateHeader());
            foreach (var updatedApp in updatedApps)
            {
                var appDataLine = generator.GenerateAppData(updatedApp);
                sb.Append(appDataLine);
                logger.Debug(appDataLine);
            }
            sb.Append(generator.GenerateFooter());

            File.WriteAllText(outputFile, sb.ToString());
            logger.Debug($"Updated app data has been written to file {outputFile}");

            return 0;
        }
    }
}
