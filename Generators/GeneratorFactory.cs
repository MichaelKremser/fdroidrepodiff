﻿using System;
using fdroidrepodiff.Enums;

namespace fdroidrepodiff.Generators
{
    /// <summary>
    /// Factory class that creates instances of generators.
    /// </summary>
    public static class GeneratorFactory
    {
        /// <summary>
        /// Returns an instance of an app list generator that can generate data in the requested format.
        /// </summary>
        /// <returns>The requested app list generator</returns>
        /// <param name="outputFormat">The format the app list generator should use.</param>
        public static IAppListGenerator GetAppListGenerator(OutputFormats outputFormat)
        {
            // TO DO CSHARP8: change to switch expression (MonoDevelop 7.8.4 does not support C# 8 supposedly)
            switch (outputFormat)
            {
                case OutputFormats.PlainText: return new PlainTextAppListGenerator();
                case OutputFormats.HTML: return new HtmlAppListGenerator();
                default: throw new NotImplementedException($"Output format {outputFormat} is not available");
            }
        }
    }
}
