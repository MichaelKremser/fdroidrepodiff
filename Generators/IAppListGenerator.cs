﻿namespace fdroidrepodiff.Generators
{
    /// <summary>
    /// Defines methods to generate a list of apps.
    /// </summary>
    public interface IAppListGenerator
    {
        /// <summary>
        /// Generates the header.
        /// </summary>
        /// <returns>The header.</returns>
        string GenerateHeader();

        /// <summary>
        /// Generates the app data.
        /// </summary>
        /// <returns>The app data.</returns>
        /// <param name="app">An instance of <see cref="Models.FDroidApplication"/> that contains data of an app that was updated.</param>
        string GenerateAppData(Models.FDroidApplication app);

        /// <summary>
        /// Generates the footer.
        /// </summary>
        /// <returns>The footer.</returns>
        string GenerateFooter();
    }
}
