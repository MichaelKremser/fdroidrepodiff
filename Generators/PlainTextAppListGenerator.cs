﻿using System;
using fdroidrepodiff.Models;

namespace fdroidrepodiff.Generators
{
    /// <summary>
    /// App list generator that generates plain text.
    /// </summary>
    public class PlainTextAppListGenerator : IAppListGenerator
    {
        internal PlainTextAppListGenerator()
        {
        }

        /// <inheritdoc />
        public string GenerateAppData(FDroidApplication app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            return $"{app.Id} was updated to {app.CurrentVersion}{Environment.NewLine}";
        }

        /// <inheritdoc />
        public string GenerateFooter() => string.Empty;

        /// <inheritdoc />
        public string GenerateHeader() => string.Empty;
    }
}
