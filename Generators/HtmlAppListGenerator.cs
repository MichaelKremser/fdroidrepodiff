﻿using System;
using fdroidrepodiff.Models;

namespace fdroidrepodiff.Generators
{
    /// <summary>
    /// App list generator that generates HTML markup.
    /// </summary>
    public class HtmlAppListGenerator : IAppListGenerator
    {
        internal HtmlAppListGenerator()
        {
        }

        /// <inheritdoc />
        public string GenerateAppData(FDroidApplication app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            return $"<tr><td><img src=\"fdroid/repo/icons/{app.Icon}\" alt=\"Icon of {app.Id}\"></td><td>{app.Id}</td><td>{app.CurrentVersion}</td></tr>";
        }

        /// <inheritdoc />
        public string GenerateFooter() => "</table>";

        /// <inheritdoc />
        public string GenerateHeader() => "<table><tr><th>&nbsp;</th><th>AppId</th><th>Version</th></tr>";
    }
}
