﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Collections;
using fdroidrepodiff.Models;

namespace fdroidrepodiff
{
    /// <summary>
    /// Represents a F droid application repository.
    /// </summary>
    public class FDroidApplicationRepository : IFDroidApplicationRepository
    {
        private Dictionary<string, FDroidApplication> applicationDictionary = new Dictionary<string, FDroidApplication>();

        /// <summary>
        /// Initializes a new instance of the <see cref="fdroidrepodiff.FDroidApplicationRepository"/> class.
        /// </summary>
        protected FDroidApplicationRepository()
        {
        }

        /// <summary>
        /// Adds a new <see cref="FDroidApplication"/> to the repository.
        /// </summary>
        /// <param name="appId">App identifier.</param>
        /// <param name="newApp">New app.</param>
        protected void Add(string appId, FDroidApplication newApp)
        {
            applicationDictionary.Add(appId, newApp);
        }

        /// <summary>
        /// Creates a repository from a XML file.
        /// </summary>
        /// <returns>Returns a new repository containing the apps of the XML file specified.</returns>
        /// <param name="Filename">Filename of a XML file that was created by the FDroid server.</param>
        public static IFDroidApplicationRepository CreateRepositoryFromXml(string Filename)
        {
            var appRepo = new FDroidApplicationRepository();
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(Filename);
            //Console.WriteLine("Processing " + Filename);
            var appNodes = xmlDoc.SelectNodes("/fdroid/application");
            Console.WriteLine("Repository " + Filename.Substring(Filename.LastIndexOf('/') + 1) + " contains " + appNodes.Count + " application" + (appNodes.Count == 1 ? "" : "s"));
            string appId = "", appName = "", appSummary = "", appDesc = "", appIcon = "";
            DateTime appAdded = DateTime.MinValue;
            DateTime appUpdated = DateTime.MinValue;
            foreach (XmlNode appNode in appNodes)
            {
                appId = appNode.SelectSingleNode("id").InnerXml;
                appName = appNode.SelectSingleNode("name").InnerXml;
                appSummary = appNode.SelectSingleNode("summary").InnerXml;
                appDesc = appNode.SelectSingleNode("desc").InnerXml;
                appIcon = appNode.SelectSingleNode("icon").InnerXml;
                appAdded = DateTime.Parse(appNode.SelectSingleNode("added").InnerXml);
                appUpdated = DateTime.Parse(appNode.SelectSingleNode("lastupdated").InnerXml);
                var newApp = new FDroidApplication()
                {
                    Id = appId,
                    Name = appName,
                    Added = appAdded,
                    LastUpdated = appUpdated,
                    CurrentVersion = GetNewestPackageVersion(appNode),
                    Summary = appSummary,
                    Desc = appDesc,
                    Icon = appIcon
                };
                appRepo.Add(appId, newApp);
            }
            return appRepo;
        }

        /// <summary>
        /// Gets the newest package version of the app described by the XML node supplied.
        /// </summary>
        /// <returns>The newest package version.</returns>
        /// <param name="appNode">A XML node that describes an app.</param>
        protected static string GetNewestPackageVersion(XmlNode appNode)
        {
            return appNode.SelectNodes("package")[0].SelectSingleNode("version").InnerText;
        }

        /// <inheritdoc />
        public IEnumerable<FDroidApplication> GetUpdatedApps(IFDroidApplicationRepository otherRepository)
        {
            if (otherRepository == null) throw new ArgumentNullException(nameof(otherRepository));

            var updatedApps = new List<FDroidApplication>();
            string appIdToCompare = "";
            FDroidApplication appNow = null;
            foreach (var app in otherRepository)
            {
                appIdToCompare = app.Value.Id;
                if (applicationDictionary.ContainsKey(appIdToCompare))
                {
                    appNow = applicationDictionary[appIdToCompare];
                    if (appNow.LastUpdated > app.Value.LastUpdated)
                    {
                        updatedApps.Add(appNow);
                    }
                }
            }
            return updatedApps;
        }

        /// <inheritdoc />
        public IEnumerator<KeyValuePair<string, FDroidApplication>> GetEnumerator()
        {
            return applicationDictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return applicationDictionary.GetEnumerator();
        }

    }
}