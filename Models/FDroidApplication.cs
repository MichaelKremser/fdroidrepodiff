﻿using System;

namespace fdroidrepodiff.Models
{
    /// <summary>
    /// Information about an application in a FDroid repository.
    /// </summary>
    public class FDroidApplication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="fdroidrepodiff.Models.FDroidApplication"/> class.
        /// </summary>
        public FDroidApplication() { }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the application was added.
        /// </summary>
        /// <value>The added.</value>
        public DateTime Added { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the application was updated.
        /// </summary>
        /// <value>The last updated.</value>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        /// Gets or sets the description of the application.
        /// </summary>
        /// <value>The desc.</value>
        public string Desc { get; set; }

        /// <summary>
        /// Gets or sets the current version of the application.
        /// </summary>
        /// <value>The current version.</value>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Gets or sets the summary of the application.
        /// </summary>
        /// <value>The summary.</value>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the icon of the application.
        /// </summary>
        /// <value>The summary.</value>
        public string Icon { get; set; }
    }
}