﻿using System.Collections.Generic;
using fdroidrepodiff.Models;

namespace fdroidrepodiff
{
    /// <summary>
    /// Provides an interface for a repository that saves information about <see cref=" FDroidApplication"/> instances.
    /// </summary>
    public interface IFDroidApplicationRepository : IEnumerable<KeyValuePair<string, FDroidApplication>>
    {
        /// <summary>
        /// Compares two repositories and returns the updated apps.
        /// </summary>
        /// <returns>A list of apps that were updated (version number was increased).</returns>
        /// <param name="otherRepository">The repository the comparision should be performed on.</param>
        IEnumerable<FDroidApplication> GetUpdatedApps(IFDroidApplicationRepository otherRepository);
    }
}